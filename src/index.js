import React from 'react';
import ReactDOM from 'react-dom';
import {Contracts} from "./Contracts";
import 'bootstrap/dist/css/bootstrap.min.css';
import {CreateContract} from "./CreateContract";

ReactDOM.render(
  <React.StrictMode>
      <CreateContract />
      <Contracts />
  </React.StrictMode>,
  document.getElementById('root')
);
