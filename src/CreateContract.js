import React from 'react';
import {Button, Container, Form, Row} from "react-bootstrap";
import {Banner} from "./Banner";

class CreateContract extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contractId: null,
            premium: null,
            startDate: null,
            displayBanner: false,
            bannerMessage: 'Failed to create contract',
            bannerVariant: 'danger',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        let name = event.target.id;
        let val = event.target.value;
        this.setState({[name]: val});
    }

    handleSubmit(event) {
        event.preventDefault();
        const { contractId, premium, startDate } = this.state
        const payload = {
            'contractId': contractId,
            'premium': premium,
            'startDate': startDate
        }

        fetch(process.env.REACT_APP_BACKEND_URL + '/contracts/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        })
            .then(r => {
                if (r.status === 200) {
                    this.setState({
                        bannerVariant: 'success',
                        bannerMessage: 'Created contract!',
                        displayBanner : true
                    })
                } else {
                    this.setState({
                        bannerVariant: 'danger',
                        bannerMessage: 'Failed to create contract',
                        displayBanner : true
                    })
                }
            })
            .catch(err => this.setState({
                bannerVariant : 'danger',
                bannerMessage: 'Failed to create contract',
                displayBanner : true
            }));
    }

    render() {
        const { displayBanner, bannerVariant, bannerMessage } = this.state
        return (
            <Container className='mt-5'>
                { displayBanner ? <Banner message={bannerMessage} variant={bannerVariant} /> : null }
                <Row md={4}>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group onChange={this.handleChange} className="mb-3" controlId="contractId">
                            <Form.Label>Contract Id</Form.Label>
                            <Form.Control type="text" placeholder="contractId"/>
                        </Form.Group>
                        <Form.Group onChange={this.handleChange} className="mb-3" controlId="premium">
                            <Form.Label>Premium</Form.Label>
                            <Form.Control type="number" placeholder="0"/>
                        </Form.Group>
                        <Form.Group onChange={this.handleChange} className="mb-3" controlId="startDate">
                            <Form.Label>Start Date</Form.Label>
                            <Form.Control type="date"/>
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Row>
            </Container>
        )
    }
}

export { CreateContract };
