import React from 'react';
import {Button, Form} from "react-bootstrap";
import PropTypes from "prop-types";
import {Banner} from "./Banner";

class TerminateContract extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            terminationDate: null,
            displayBanner: false,
            bannerMessage: 'Failed to terminate contract',
            bannerVariant: 'danger',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        let name = event.target.id;
        let val = event.target.value;
        this.setState({[name]: val});
    }

    handleSubmit(event) {
        event.preventDefault();
        const { terminationDate } = this.state
        const { contractId } = this.props
        const payload = {
            'contractId': contractId,
            'terminationDate': terminationDate
        }

        fetch(process.env.REACT_APP_BACKEND_URL + '/contracts/terminate', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        })
            .then(r => {
                if (r.status === 200) {
                    this.setState({
                        bannerVariant: 'success',
                        bannerMessage: 'Terminated contract!',
                        displayBanner : true
                    })
                } else {
                    this.setState({
                        bannerVariant: 'danger',
                        bannerMessage: 'Failed to terminate contract',
                        displayBanner : true
                    })
                }
            })
            .catch(err => this.setState({
                bannerVariant : 'danger',
                bannerMessage: 'Failed to terminate contract',
                displayBanner : true
            }));
    }

    render() {
        const { displayBanner, bannerVariant, bannerMessage } = this.state
        return (
            <div>
                { displayBanner ? <Banner message={bannerMessage} variant={bannerVariant} /> : null }
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group onChange={this.handleChange} className="mb-3" controlId="terminationDate">
                        <Form.Control type="date"/>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        )
    }
}

TerminateContract.propTypes = {
    contractId: PropTypes.string.isRequired
};

export { TerminateContract };
