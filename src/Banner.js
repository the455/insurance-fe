import React from 'react';
import {Alert} from "react-bootstrap";
import PropTypes from "prop-types";

class Banner extends React.Component {
    render() {
        return (
            <Alert variant={this.props.variant}>
                {this.props.message}
            </Alert>
        )
    }
}

Banner.propTypes = {
    message: PropTypes.string.isRequired,
    variant: PropTypes.string.isRequired
};

export { Banner };
