import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table'
import {TerminateContract} from "./TerminateContract";
import {Container} from "react-bootstrap";

function Contracts() {
    const [contracts, setContracts] = useState([]);

    useEffect(() => {
        fetch(process.env.REACT_APP_BACKEND_URL + '/contracts')
            .then(response => response.json())
            .then(data => setContracts(data));
    }, []);

    function renderRow(x) {
        const {contractId, premium, startDate, terminationDate} = x
        return (
            <tr key={contractId}>
                <td>{contractId}</td>
                <td>{premium}</td>
                <td>{startDate}</td>
                <td>{terminationDate ? terminationDate : <TerminateContract contractId={contractId} />}</td>
            </tr>
        );
    }

    function renderTable() {
        return (
            <Container className='mt-5'>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>Contract Id</th>
                        <th>Premium</th>
                        <th>Start Date</th>
                        <th>Termination Date</th>
                    </tr>
                    </thead>
                    <tbody>
                        {contracts.map(x => renderRow(x))}
                    </tbody>
                </Table>
            </Container>
        )
    }

    return (
        <div>
            {renderTable()}
        </div>
    );
}

export { Contracts };
