# Insurance Frontend

This is the contract administration tool.

It is currently configurable via url, which is currently pointing at a hosted backend service.
If you wish to point to a locally running backend service then just change the url to `http://localhost:8080`

# Improvements

- Currently, when adding a new contract only a banner is shown. Would have been nice to update the table when the request has completed.

- The banners also remain forever in their current implentation, would be nice t replace them with a pop up alert that disappears after a few seconds.
